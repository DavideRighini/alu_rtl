#include <systemc.h>
#include "MUX_4x16.hpp"

using namespace std;

void MUX_4x16::select()
{

   while(true)
   {
     wait();
     switch (sel->read().to_uint())
     {
       case 5 : Y->write(X0); break;
       case 15 : Y->write(X1); break;
       case 20 : Y->write(X2); break;
       case 10 : Y->write(X3); break;
      }
   }

}
