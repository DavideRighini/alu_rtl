#include <systemc.h>
#include "sommatore.hpp"

using namespace std;

void sommatore::somma()
{
   sc_lv<16> res;
   while(true)
   {
     wait();
     res=src1->read().to_uint()+src2->read().to_uint(); 
     out->write(res);
   }
}
