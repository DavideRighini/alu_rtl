#include <systemc.h>
#include "ALU_RTL.hpp"

using namespace std;

void ALU_RTL::update_ports() {
	result->write(0); //inizializzo uscita
	
	while(true) {
		wait();
		wait(1,SC_NS); //ritardo
		if (result_tmp.read().to_uint() == 0) zero->write(1);
		else zero->write(0);
		
		result->write(result_tmp.read().to_int());
		wait(SC_ZERO_TIME);	
	}
}

void ALU_RTL::update_in_ports() {
	while(true) {
		wait();
		wait(5,SC_NS); //ritardo
		
		op1_tmp.write(op1->read());
		op2_tmp.write(op2->read());
		func_tmp.write(func->read());

		wait(SC_ZERO_TIME);
		
	}
	

}
