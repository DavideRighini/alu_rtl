#ifndef ALU_RTL_HPP
#define ALU_RTL_HPP

#include "comparatore.hpp"
#include "nand.hpp"
#include "sommatore.hpp"
#include "MUX_4x16.hpp"

using namespace std;

SC_MODULE(ALU_RTL)
{
   sc_in<sc_lv<16> > op1;
   sc_in<sc_lv<16> > op2;
   sc_in<sc_lv<6> > func;
   sc_out<sc_lv<16> > result;
	 sc_out<bool> zero;
	 
   sc_signal<sc_lv<16> > adder_out,comp_out,nand_out;
	 sc_signal<sc_lv<16> > result_tmp, op1_tmp, op2_tmp;
	 sc_signal<sc_lv<6> > func_tmp;

   comparatore comp;
   nand nand1;
   sommatore adder;
   MUX_4x16 mux;

	 SC_CTOR(ALU_RTL) : comp("comp"), nand1("nand1"), adder("adder"), mux("mux")
	  {
			
			SC_THREAD(update_ports);
			sensitive << result_tmp;
			dont_initialize();
		
			SC_THREAD(update_in_ports);
			sensitive << op1 << op2 << func;
			dont_initialize();
					
		  //Collegamenti del comparatore
		  comp.src1(this->op1_tmp);
		  comp.src2(this->op2_tmp);
		  comp.out(this->comp_out);
		  //Collegmaneti della nand
		  nand1.src1(this->op1_tmp);
		  nand1.src2(this->op2_tmp);
		  nand1.out(this->nand_out);
		  //Collegamenti del sommatore
		  adder.src1(this->op1_tmp);
		  adder.src2(this->op2_tmp);
		  adder.out(this->adder_out);
		  //Collegamenti del mux
		  mux.X0(this->adder_out);
		  mux.X1(this->nand_out);
		  mux.X2(this->op1_tmp);
		  mux.X3(this->comp_out);
		  mux.sel(this->func_tmp);
		  mux.Y(this->result_tmp);		
	 }
	
	private:
	void update_ports();
	void update_in_ports();

};


#endif
