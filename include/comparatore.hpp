#ifndef COMPARATORE_HPP
#define COMPARATORE_HPP

SC_MODULE(comparatore)
{
   sc_in<sc_lv<16> >  src1;
   sc_in<sc_lv<16> >  src2;
   sc_out<sc_lv<16> > out;

   SC_CTOR(comparatore)
   {
     SC_THREAD(compara);
     sensitive << src1 << src2;
   }
   private:
   void compara ();
};


#endif
