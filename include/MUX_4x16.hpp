#ifndef MUX_4x16_HPP
#define MUX_4x16_HPP

SC_MODULE(MUX_4x16)
{
   sc_in<sc_lv<16> >  X0;
   sc_in<sc_lv<16> >  X1;
   sc_in<sc_lv<16> >  X2;
   sc_in<sc_lv<16> >  X3;
   sc_in<sc_lv<6> >   sel;
   sc_out<sc_lv<16> > Y;

   SC_CTOR(MUX_4x16)
   {
     SC_THREAD(select);
     sensitive << X0 << X1 << X2 << X3 << sel;
   }
   private:
   void select ();
};


#endif
