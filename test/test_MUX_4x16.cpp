#include <systemc.h>
#include <string>
#include "MUX_4x16.hpp"

using namespace std;

SC_MODULE(TestBench)
{
   sc_signal<sc_lv<16> > ingresso0;
   sc_signal<sc_lv<16> > ingresso1;
   sc_signal<sc_lv<16> > ingresso2;
   sc_signal<sc_lv<16> > ingresso3;
   sc_signal<sc_lv<6> >  selezione;
   sc_signal<sc_lv<16> > uscita;
   MUX_4x16 mux;

   SC_CTOR(TestBench) : mux("mux")
   {
        SC_THREAD(stimulus_thread);
        mux.X0(ingresso0);
        mux.X1(ingresso1);
        mux.X2(ingresso2);
        mux.X3(ingresso3);
        mux.sel(selezione);
        mux.Y(uscita);
        init_values();
   }

   int check()
   {
        for (unsigned i=0;i<TEST_SIZE;i++) 
        { 

            if (dato_letto[i] != out_test[i])
            {
                cout << "TEST FALLITO: " << i << endl;
                cout << "RISULTATO TEORICO: " << out_test[i] << endl;
                cout << "RISULTATO TEST :" << dato_letto[i] << endl;
                return 1;
             }
        }
        cout << "TEST OK" << endl;                 
        return 0;
   }

   private:

   void stimulus_thread()
   {
      for (unsigned i=0;i<TEST_SIZE;i++) 
      {
        ingresso0.write(in0_test[i]);
        ingresso1.write(in1_test[i]);
        ingresso2.write(in2_test[i]);
        ingresso3.write(in3_test[i]);
        selezione.write(sel_test[i]);
        cout << "IN0 = " << in0_test[i] << endl;
        cout << "IN1 = " << in1_test[i] << endl;
        cout << "IN2 = " << in2_test[i] << endl;
        cout << "IN3 = " << in3_test[i] << endl;
        cout << "SEL = " << sel_test[i] << endl;
        wait(1,SC_NS);
        dato_letto[i]=uscita.read().to_uint();
        cout << "OUT = " << dato_letto[i] << endl << endl;
        
      }
   }

   static const unsigned TEST_SIZE = 4;
   unsigned in0_test[TEST_SIZE];
   unsigned in1_test[TEST_SIZE];
   unsigned in2_test[TEST_SIZE];
   unsigned in3_test[TEST_SIZE];
   unsigned sel_test[TEST_SIZE];
   unsigned out_test[TEST_SIZE];
   unsigned dato_letto[TEST_SIZE];
   void init_values()
   {
      in0_test[0] = 0;
      in0_test[1] = 1;
      in0_test[2] = 23;
      in0_test[3] = 666;

      in1_test[0] = 1;
      in1_test[1] = 0;
      in1_test[2] = 32;
      in1_test[3] = 50;

      in2_test[0] = 7;
      in2_test[1] = 39;
      in2_test[2] = 2;
      in2_test[3] = 34;

      in3_test[0] = 8;
      in3_test[1] = 40;
      in3_test[2] = 3;
      in3_test[3] = 35;

      sel_test[0] = 5;
      sel_test[1] = 15;
      sel_test[2] = 20;
      sel_test[3] = 10;

      for (unsigned i=0;i<TEST_SIZE;i++)
      {
        switch (sel_test[i])
        {
           case 5 : out_test[i]=in0_test[i]; break;
           case 15 : out_test[i]=in1_test[i]; break;
           case 20 : out_test[i]=in2_test[i]; break;
           case 10 : out_test[i]=in3_test[i]; break;
         }
      }
   }
};

int sc_main(int argc, char* argv[])
{
  TestBench test("test");

  cout << "START TEST" << endl << endl;

  sc_start();

  return test.check();
}
