#include <systemc.h>
#include <string>
#include "ALU_RTL.hpp"

using namespace std;

SC_MODULE(TestBench)
{
    sc_signal<sc_lv<16> > operand1;
    sc_signal<sc_lv<16> > operand2;
    sc_signal<sc_lv<6> >  controllo;
    sc_signal<sc_lv<16> > result;
    sc_signal<bool> zero;
    ALU_RTL ALU1;
    
    SC_CTOR(TestBench) : ALU1("ALU1")
    {
        SC_THREAD(stimulus_thread);
        ALU1.op1(this->operand1);
        ALU1.op2(this->operand2);
        ALU1.result(this->result);
        ALU1.func(this->controllo);
        ALU1.zero(this->zero);
        init_values();
    }

    int check() 
    {

        for (unsigned i=0;i<TEST_SIZE;i++) 
        {
            if (dato_letto[i] != risultato_test[i].to_uint() & zero_letto[i] != zero_test[i] )
            {
                cout << "TEST FALLITO: " << i << endl;
                cout << "RISULTATO TEORICO: " << risultato_test[i] << endl;
                cout << "RISULTATO TEST :" << dato_letto[i] << endl;
                return 1;
             }
        }
        cout << "TEST OK" << endl;                 
        return 0;
    }


  private:

   void stimulus_thread() 
   {
        for (unsigned i=0;i<TEST_SIZE;i++) 
        {
            operand1.write(op1_test[i]);
            cout << "Operando 1 = " << op1_test[i] << endl;
            operand2.write(op2_test[i]);
            cout << "Operando 2 = " << op2_test[i] << endl;
            controllo.write(controllo_test[i]);
            cout << "Funzione = ";
            switch (controllo_test[i])
            {
              case 5 : cout << "ADD" << endl; break;
              case 15 : cout << "NAND" << endl; break;
              case 20 : cout << "PASS1" << endl; break;
              case 10 : cout << "EQ?" << endl; break;
            }
            wait(10,SC_NS);
            dato_letto[i] = result.read().to_uint(); 
            zero_letto[i] = zero.read();
            cout << "DATO LETTO : " << dato_letto[i] << " Zero letto: " << zero_letto[i] << endl << endl;
        }
    }

    static const unsigned TEST_SIZE = 8;
    // NOTA: short = 2 byte
    unsigned op1_test[TEST_SIZE];
    unsigned op2_test[TEST_SIZE];
    unsigned controllo_test[TEST_SIZE];
    sc_lv<16> risultato_test[TEST_SIZE];
    bool zero_test[TEST_SIZE];
    unsigned dato_letto[TEST_SIZE];
    unsigned zero_letto[TEST_SIZE];           

    void init_values() 
    {
        op1_test[0] = 1;
        op1_test[1] = 5;
        op1_test[2] = 0;
        op1_test[3] = 4;
        op1_test[4] = 0;
        op1_test[5] = 239;
        op1_test[6] = 666;
        op1_test[7] = 23;

        op2_test[0] = 0;
        op2_test[1] = 3;
        op2_test[2] = 0;
        op2_test[3] = 0;
        op2_test[4] = 2;
        op2_test[5] = 2;
        op2_test[6] = 666;
        op2_test[7] = 37;

        controllo_test[0]=5;
        controllo_test[1]=5;
        controllo_test[2]=15;
        controllo_test[3]=15;
        controllo_test[4]=20;
        controllo_test[5]=20;
        controllo_test[6]=10;
        controllo_test[7]=10;

        for (unsigned i=0;i<TEST_SIZE;i++)
        {
          switch (controllo_test[i])
          {
            case 5 : 
            				risultato_test[i]=op1_test[i]+op2_test[i]; 
            				if (risultato_test[i] == 0) zero_test[i]=1;
            				else zero_test[i]=0;
            				break; //ADD
            case 15 : 
            				risultato_test[i]=~(op1_test[i]&op2_test[i]); 
            				if (risultato_test[i] == 0) zero_test[i]=1;
            				else zero_test[i]=0;
            				break; //NAND
            case 20 : 
            				risultato_test[i]=op1_test[i]; 
            				if (risultato_test[i] == 0) zero_test[i]=1;
            				else zero_test[i]=0;
            				break; //PASS1
            case 10 : 
            				if (op1_test[i]==op2_test[i])
	               		risultato_test[i]=0;
                    else
                       risultato_test[i]=op1_test[i] ^ op2_test[i];
             				if (risultato_test[i] == 0) zero_test[i]=1;
            				else zero_test[i]=0;                   
                    break;     //EQ?
          }
        }
    }


};

int sc_main(int argc, char* argv[])
{
  TestBench test("test");

  cout << "START TEST" << endl << endl;

  sc_start();

  return test.check();
}
