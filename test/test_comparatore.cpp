#include <systemc.h>
#include <string>
#include "comparatore.hpp"

using namespace std;

SC_MODULE(TestBench)
{
    sc_signal<sc_lv<16> > operand1;
    sc_signal<sc_lv<16> > operand2;
    sc_signal<sc_lv<16> > result;
    comparatore comp;
    
    SC_CTOR(TestBench) : comp("comp")
    {
        SC_THREAD(stimulus_thread);
        comp.src1(this->operand1);
        comp.src2(this->operand2);
        comp.out(this->result);
        init_values();
    }

    int check() 
    {

        for (unsigned i=0;i<TEST_SIZE;i++) 
        {
            if (dato_letto[i] != risultato_test[i])
            {
                cout << "TEST FALLITO: " << i << endl;
                cout << "RISULTATO TEORICO: " << risultato_test[i] << endl;
                cout << "RISULTATO TEST :" << dato_letto[i] << endl;
                return 1;
             }
        }
        cout << "TEST OK" << endl;                 
        return 0;
    }


  private:

   void stimulus_thread() 
   {
        for (unsigned i=0;i<TEST_SIZE;i++) 
        {
            operand1.write(op1_test[i]);
            cout << "Operando 1 = " << op1_test[i] << endl;
            operand2.write(op2_test[i]);
            cout << "Operando 2 = " << op2_test[i] << endl;
            wait(1,SC_NS);
            dato_letto[i] = result.read().to_uint(); 
            cout << "DATO LETTO : " << dato_letto[i] << endl << endl;
        }
    }

    static const unsigned TEST_SIZE = 8;
    // NOTA: short = 2 byte
    unsigned op1_test[TEST_SIZE];
    unsigned op2_test[TEST_SIZE];
    sc_lv<16> risultato_test[TEST_SIZE];
    unsigned dato_letto[TEST_SIZE];

    void init_values() 
    {
        op1_test[0] = 1;
        op1_test[1] = 5;
        op1_test[2] = 0;
        op1_test[3] = 4;
        op1_test[4] = 0;
        op1_test[5] = 239;
        op1_test[6] = 666;
        op1_test[7] = 65535;

        op2_test[0] = 0;
        op2_test[1] = 5;
        op2_test[2] = 0;
        op2_test[3] = 0;
        op2_test[4] = 2;
        op2_test[5] = 2;
        op2_test[6] = 666;
        op2_test[7] = 65535;

        for (unsigned i=0;i<TEST_SIZE;i++)
           if (op1_test[i]==op2_test[i])
             risultato_test[i]=0;
           else
             risultato_test[i]=(op1_test[i] ^ op2_test[i]);

    }


};

int sc_main(int argc, char* argv[])
{
  TestBench test("test");

  cout << "START TEST" << endl << endl;

  sc_start();

  return test.check();
}
