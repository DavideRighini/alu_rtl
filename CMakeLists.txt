cmake_minimum_required (VERSION 2.6)

include_directories ("include")


add_library (sommatore "src/sommatore.cpp")
add_library (nand "src/nand.cpp")
add_library (comparatore "src/comparatore.cpp")
add_library (mux_mm "src/MUX_4x16.cpp")
add_library (ALU_RTL SHARED "src/ALU_RTL.cpp")

add_executable (test_sommatore test/test_sommatore.cpp)
add_executable (test_nand test/test_nand.cpp)
add_executable (test_comparatore test/test_comparatore.cpp)
add_executable (test_MUX_4x16 test/test_MUX_4x16.cpp)
add_executable (test_ALU_RTL test/test_ALU_RTL.cpp)

target_link_libraries (test_sommatore sommatore systemc)
target_link_libraries (test_nand nand systemc)
target_link_libraries (test_comparatore comparatore systemc)
target_link_libraries (test_MUX_4x16 mux_mm systemc)
target_link_libraries (test_ALU_RTL ALU_RTL mux_mm comparatore nand sommatore systemc)

enable_testing ()

add_test (sommatore test_sommatore)
add_test (nand test_nand)
add_test (comparatore test_comparatore)
add_test (mux test_MUX_4x16)
add_test (ALU_RTL test_ALU_RTL)
